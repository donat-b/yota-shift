{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings#-}

module Main where

import Lib
import Control.Concurrent
import Control.Monad
import Turtle
import qualified Control.Foldl as Fold

samplingTime :: Integer
samplingTime = 10

minPlan :: Text
minPlan = "512"

maxPlan :: Text
maxPlan = "2.1"

measureFlow :: IO Integer
measureFlow = do
  rStart <- getDecimal "ReceivedBytes"
  threadDelay . fromInteger $ 1000000 * samplingTime
  rEnd <- getDecimal "ReceivedBytes"
  let diff = rEnd - rStart
  return $ div diff samplingTime

getCurrentPlan :: IO (Maybe Line)
getCurrentPlan = do
  let cmd = "yota -L | grep '\\[active\\]' | awk '{print $2}'"
  fold (inshell cmd empty) Fold.head

setPlan :: Text -> IO ()
setPlan speed = do
  let cmd = "yota -C -s " <> speed
  x <- fold (inshell cmd empty) Fold.head
  case x of
    Just "tariff successfully changed" -> return ()
    Nothing -> error "oops in setPlan"

upShift :: Integer -> IO ()
upShift rate = do
  if (rate > 60000) 
     then setPlan maxPlan
     else putStrLn "upShift: unchanged"

downShift :: Integer -> IO ()
downShift rate = do
  if (rate < 5120)
     then setPlan minPlan
     else putStrLn "downShift: unchanged"

main :: IO ()
main = do
  c <- getString "State"
  when (c /= "Connected") (error "Not connected, can't continue")

  rate <- measureFlow

  plan <- getCurrentPlan
  case plan of
    Just "512" -> upShift rate
    Just maxPlan -> downShift rate
    Nothing -> error "oops"
