module Lib
    ( getStatus
    , getDecimal
    , getString
    ) where

import Network.HTTP

statusURL :: [Char]
statusURL = "http://forseti.lan/cgi-bin/sysconf.cgi?page=ajax&action=get_status"

getStatus :: IO String
getStatus = do
  rsp <- Network.HTTP.simpleHTTP (getRequest statusURL)
  getResponseBody rsp

getDecimal :: [Char] -> IO Integer
getDecimal key = do
  statusRaw <- getStatus
  let res = map (fmap (drop 1) . break (=='=')) $ lines statusRaw
  case lookup key res of
    Nothing -> error "Key not found"
    Just x -> return (read x)

getString :: [Char] -> IO [Char]
getString key = do
  statusRaw <- getStatus
  let res = map (fmap (drop 1) . break (=='=')) $ lines statusRaw
  case lookup key res of
    Nothing -> error "Key not found"
    Just x -> return x
